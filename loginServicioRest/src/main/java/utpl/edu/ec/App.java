package utpl.edu.ec;

/**
 * Hello world!
 *
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Generar la aplicación Web no segura

    Antes de asegurar la aplicación web, comprobar que funciona. 
    Para ello, es necesario definir algunos granos clave. 
    Para ello, cree una Applicationclase.

    src/main/java/com/utpl/edu/ec/aplicacion.java
 * 
 */

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    
    }
}


