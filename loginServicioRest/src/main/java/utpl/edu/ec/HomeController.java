/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utpl.edu.ec;

/**
 *  Crear un controlador simple de la tela

    En primavera, los puntos finales REST son sólo los controladores Spring MVC. 
    El siguiente controlador de Spring MVC maneja una GET /petición de devolución de un mensaje simple:

    src/main/java/hello/HomeController.java
 */
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String index() {
        return "Bienvenido al Servicio Web Restful (Metodo de autenticación y encriptación)";
    }
}

/**
 * La clase entera está marcada con @RestControllertan Spring MVC 
 * puede detectar automáticamente el controlador con que está construido 
 * en funciones de escaneado y configurar automáticamente las rutas web.
 * 
 * El método se etiqueta con @RequestMappingmarcar la ruta y la acción REST. 
 * En este caso, GETes el comportamiento por defecto; 
 * devuelve un mensaje indicando que se encuentra en la página principal.
 * 
 * 
 * 
 */