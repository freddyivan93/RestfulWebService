# Servicios Web Restful con Spring Security (Login)

> **Materia:** Arquitectura de Aplicaciones UTPL

> **Tema:** Desarrollo de una aplicación  de servicios web restful con el framework Spring Security.

## 1. Objetivo:
> Desarrollar una aplicación de servicios web Restful para la autenticación de usuarios mediante el framework Spring Security para su aseguramiento con ayuda de la plataforma NetBeans.

## 2. Estructura

> ![estructura](/uploads/d7a8614bb5d317b3513e9737bf2d9b58/estructura.PNG)

## 3. Desarrollo

### 3.1 Crear un controlador web simple (**HomeController.java**)

 En Spring, los puntos finales REST son sólo los controladores Spring MVC. El siguiente controlador de Spring MVC maneja una petición **GET /** de devolución de un mensaje simple:

    src/main/java/utpl/edu/ec/HomeController.java
    
```java
package utpl.edu.ec;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String index() {
        return "Bienvenido al Servicio Web Restful (Metodo de autenticación y encriptación)";
    }
}
```
> **@RestController**

> La clase entera está marcada con @RestControllertan Spring MVC puede detectar automáticamente el controlador con que está construido en funciones de escaneado y configurar automáticamente las rutas web.

> **@RequestMapping**

> El método se etiqueta con @RequestMappingmarcar la ruta y la acción REST. En este caso, GETes el comportamiento por defecto; devuelve un mensaje indicando que se encuentra en la página principal.

### 3.2 Generar la aplicación Web no segura (**Aplicacion.java**)

Antes de asegurar la aplicación web, hay que comprobar que funciona. Para ello, es necesario definir algunos claves beans. Para ello, crear una clase **Aplicacion.java**.

    src/main/java/com/utpl/edu/ec/Aplicacion.java

```java
package utpl.edu.ec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Aplicacion 
{
    public static void main( String[] args )
    {
        SpringApplication.run(Aplicacion.class, args);
    
    }
}

```
### 3.3 Creación de las dependencias para Maven

Dentro del archivo **pom.xml** asignar las siguientes dependencias:

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-ldap</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.directory.server</groupId>
        <artifactId>apacheds-server-jndi</artifactId>
        <version>1.5.5</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework.security</groupId>
        <artifactId>spring-security-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```
### 3.4 Creacion de una clase **WebSecurityConfig**

Que se encargara de toda la configuración de la seguridad web.

```java
package utpl.edu.ec;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().fullyAuthenticated()
				.and()
			.formLogin();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.groupSearchBase("ou=groups")
				.contextSource().ldif("classpath:test-server.ldif");
	}
}
```
> **@EnableWebSecurity**

> El @EnableWebSecurity activa una variedad de beans necesarios para usar Spring Security.

> También necesita un servidor LDAP. El módulo LDAP de Spring Security incluye un servidor incrustado escrito en Java puro, que se está utilizando para esta guía. El método **ldapAuthentication()** configura las cosas donde el nombre de usuario en el formulario de inicio de sesión está conectado a **{0}** de forma que busque **uid = {0}, ou = people, dc = springframework, dc = org** en el servidor LDAP.

### 3.5 Configuración de los datos de usuario (**test-server.ldif**)

Los servidores LDAP pueden utilizar archivos LDIF (LDAP Data Interchange Format) para intercambiar datos de usuario. El método **ldif()** dentro de **WebSecurityConfig** extrae un archivo de datos LDIF. Esto facilita la pre-carga de datos de demostración.

Para ello creamos un archivo **test-server.ldif**, donde va la información de los usuarios (nombre y contraseña), en este caso mi **constraseña esta encrriptada = "freddy" y el usuario "freddy "**.

    src/main/resources/test-server.ldif

```ldif
dn: ou=groups,dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: groups

dn: ou=subgroups,ou=groups,dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: subgroups

dn: ou=people,dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: people

dn: ou=space cadets,dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: space cadets

dn: ou=\"quoted people\",dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: "quoted people"

dn: ou=otherpeople,dc=springframework,dc=org
objectclass: top
objectclass: organizationalUnit
ou: otherpeople

dn: uid=freddy,ou=people,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: freddy Alex
sn: Alex
uid: freddy
userPassword: {SHA}XIp6Ep3otknpoMv7t+nOw3pu/LY= 

dn: uid=bob,ou=people,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: Bob Hamilton
sn: Hamilton
uid: bob
userPassword: bobspassword

dn: uid=joe,ou=otherpeople,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: Joe Smeth
sn: Smeth
uid: joe
userPassword: joespassword

dn: cn=mouse\, jerry,ou=people,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: Mouse, Jerry
sn: Mouse
uid: jerry
userPassword: jerryspassword

dn: cn=slash/guy,ou=people,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: slash/guy
sn: Slash
uid: slashguy
userPassword: slashguyspassword

dn: cn=quote\"guy,ou=\"quoted people\",dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: quote\"guy
sn: Quote
uid: quoteguy
userPassword: quoteguyspassword

dn: uid=space cadet,ou=space cadets,dc=springframework,dc=org
objectclass: top
objectclass: person
objectclass: organizationalPerson
objectclass: inetOrgPerson
cn: Space Cadet
sn: Cadet
uid: space cadet
userPassword: spacecadetspassword

dn: cn=developers,ou=groups,dc=springframework,dc=org
objectclass: top
objectclass: groupOfNames
cn: developers
ou: developer
uniqueMember: uid=freddy,ou=people,dc=springframework,dc=org
uniqueMember: uid=bob,ou=people,dc=springframework,dc=org

dn: cn=managers,ou=groups,dc=springframework,dc=org
objectclass: top
objectclass: groupOfNames
cn: managers
ou: manager
uniqueMember: uid=freddy,ou=people,dc=springframework,dc=org
uniqueMember: cn=mouse\, jerry,ou=people,dc=springframework,dc=org

dn: cn=submanagers,ou=subgroups,ou=groups,dc=springframework,dc=org
objectclass: top
objectclass: groupOfNames
cn: submanagers
ou: submanager
uniqueMember: uid=galo,ou=people,dc=springframework,dc=org
```

## 4. Ejecución

Ejecutamos la **Aplicacion.java** 

> ![spring](/uploads/5beaf76418e8162097886102696d4018/spring.PNG) 

Luego abrimos en el navegador mediante **localhost:8080**

> ![login](/uploads/13644cf634f905fcd6c00b97d2d9e07d/login.PNG) 

Y si la contrasena es la correcta nos enviara a la pagina de Bienvenida.

> ![bienvenida](/uploads/508d9868ebca83ab2102e5800f6035bd/bienvenida.PNG)

Pero si la contrasena o el usuario no estan registrados nos impedira entrar.

> ![nologin](/uploads/51122edcf7a39475488762b93f8cb334/nologin.PNG)

## 5. Referencias

Spring Security [Spring Security](https://spring.io/guides/gs/authenticating-ldap/)

